//Web Model
var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');

var Schema = mongoose.Schema;

let rolesValidos = {
    values: ['USER_ROLE', 'ADMIN_ROLE',],
    message: 'Error, expected {PATH} is not valid.'

}

var contactoSchema = new Schema({

    nombre: {  type: String, required: [false, ' El nombre es requerido']},
    apellido: {  type: String, required: [false, ' El apellido es requerido']},

    email: {  type: String, required: [false, ' El email es requerido']},
    telefono: {  type: String, required: [false, ' El telefono es requerido']},
    mensaje: {  type: String, required: [false, ' El mensaje es requerido']},
    


});

contactoSchema.plugin(uniqueValidator, { message: 'Error, expected {PATH} to be unique.' });


module.exports = mongoose.model('Contacto', contactoSchema);
