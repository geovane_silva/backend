'use strict'

//Web Model
var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');

var Schema = mongoose.Schema;

let rolesValidos = {
    values: ['USER_ROLE', 'ADMIN_ROLE',],
    message: 'Error, expected {PATH} is not valid.'

}

var descuentoSchema = new Schema({

    nombre: {  type: String, required: [false, ' El nombre es requerido']},
    img: {  type: String, required: [false, ' El img es requerido']},


    


});

descuentoSchema.plugin(uniqueValidator, { message: 'Error, expected {PATH} to be unique.' });


module.exports = mongoose.model('Descuento', descuentoSchema);
