//Web Model
var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');

var Schema = mongoose.Schema;

let rolesValidos = {
    values: ['USER_ROLE', 'ADMIN_ROLE',],
    message: 'Error, expected {PATH} is not valid.'

}

var ventaSchema = new Schema({

    nombre: {  type: String, required: [false, ' El nombre es requerido']},
    apellido: {  type: String, required: [false, ' El apellido es requerido']},
    email: {  type: String, required: [false, ' El email es requerido']},
    telefono: {  type: String, required: [false, ' El telefono es requerido']},
    marca: {  type: String, required: [false, ' El marca es requerido']},
    modelo: {  type: String, required: [false, ' El modelo es requerido']},
    ano: {  type: String, required: [false, ' El ano es requerido']},
    description_transmision: {  type: String, required: [false, ' El description_transmision es requerido']},
    precio_venta: {  type: String, required: [false, ' El precio_venta es requerido']},
    fotos: {  type: Array, required: [false, ' El precio_venta es requerido']},



    foto_frontal: {  type: String, required: [false, ' El foto_frontal es requerido']},
    foto_trasera: {  type: String, required: [false, ' El foto_trasera es requerido']},
    foto_izquierda: {  type: String, required: [false, ' El foto_izquierda es requerido']},
    foto_derecha: {  type: String, required: [false, ' El foto_derecha es requerido']},
    foto_motor: {  type: String, required: [false, ' El foto_motor es requerido']},
    foto_interior: {  type: String, required: [false, ' El foto_interior es requerido']},

    role: { type: String, enum: rolesValidos, required: false, default: 'USER_ROLE'},



});

ventaSchema.plugin(uniqueValidator, { message: 'Error, expected {PATH} to be unique.' });


module.exports = mongoose.model('Venta', ventaSchema);
