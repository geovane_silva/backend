'use strict'

//Web Model
var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');

var Schema = mongoose.Schema;

let rolesValidos = {
    values: ['USER_ROLE', 'ADMIN_ROLE',],
    message: 'Error, expected {PATH} is not valid.'

}

var subscribeSchema = new Schema({

    email: {  type: String, required: [true, ' El email es requerido']},
    nombre: {  type: String, required: [true, ' El nombre es requerido']},

    


});

subscribeSchema.plugin(uniqueValidator, { message: 'Error, expected {PATH} to be unique.' });


module.exports = mongoose.model('Suscribe', subscribeSchema);
