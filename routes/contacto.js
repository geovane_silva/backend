'use strict'

const express =  require('express');

const contactoController = require('../controllers/contacto');


let router = express.Router();


router.post('/contacto', contactoController.postContact);



module.exports = router;