'use strict'

const express = require('express');
const userController = require('../controllers/user')
const service = require('../services/email')



// Init Var
let router = express.Router();
 
let  auth = require('../middlewares/auth') // auth


router.post('/register',[auth.verificaToken ,auth.verificaRole_Admin], userController.registerUser); // post register user

router.post('/login', userController.login); // post login user

router.get('/user' , userController.getUsers); // get list users

router.put('/user/:id', userController.updateUser); // updatelist user











module.exports = router;