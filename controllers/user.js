"use strict";
// modules
const bcrypt = require("bcrypt");
const jwt = require('jsonwebtoken');

//model
var User = require("../database/models/user");




//=====================================
//           GET USERS = GET
//=====================================


function getUsers(req, res) {
  let desde = req.query.desde || 0;
  desde = Number(desde);

  let limite = req.query.limite || 5;
  limite = Number(limite);
  

  //  Usuario.find({ estado: true },'id name img role email')  nao eliminar usuario, apenas cambiar de estado

  User.find({}, 'id img  email nombre role unidad asociacion direccion companyAdmin')
    .skip(desde)
    .limit(limite)
    .exec((err, users) => {
      if (err) {
        return res.status(500).json({
          ok: false,
          message:
            "Something on the server didnt work right. Please, You need Verify your information id",
          err,
        });
      }

         
         
      
      //Usuario.count({}, (err, counts)=>{ estado para dar os filtros apenas de usuarios ativos
      User.count({}, (err, counts) => {
        //jqueri para contar usuarios
        return res.status(200).json({
          ok: true,
          users: users,
          TotalUsers: counts,
        });
      });
    });
}

//=====================================
//           SAVE USER  = POST
//=====================================

function registerUser(req, res) {
  var body = req.body; // create object user
  var user = new User({
    nombre: body.nombre, // This is very diferente od the post, do you need add model gona be update
    direccion: body.direccion,
    companyAdmin: body.companyAdmin,
    asociacion: body.asociacion,
    unidad: body.unidad,
    email: body.email,
    password: bcrypt.hashSync(body.password, 10),
    img: body.img,
    estado: body.estado,
    role: body.role,
  });

  user.save((err, userStored, next) => {
    if (err) {
      return res.status(500).json({
        ok: false,
        message: "Something on the server didnt work right.",
        err,
      });
    }
    if (!userStored) {
      res.status(404).json({
        ok: false,
        message: "A file doesnt exist at that address",
        user: userStored,
      });
    }

    return res.status(201).json({
      ok: true,
      message: 'Everything is normal ("201 Created")',
      user: userStored,
    });
  });
}

//=====================================
//         UPDATE USER = PUT
//=====================================

function updateUser(req, res) {
  let id = req.params.id;
  let body = req.body;

  User.findById(id, (err, user) => {
    if (err) {
      return res.status(500).json({
        ok: false,
        message: "Something on the server didnt work right put.",
        err,
      });
    }

    if (!user) {
      res.status(404).json({
        ok: false,
        message: "A file doesnt exist at that address",
        user,
      });
    }

      (user.nombre = body.nombre), // This is very diferente od the post, do you need add model gona be update
      (user.direccion = body.direccion),
      (user.companyAdmin = body.companyAdmin),
      (user.asociacion = body.asociacion),
      (user.unidad = body.unidad),

      (user.email = body.email),
      (user.password = body.password),
      (user.img = body.img),
      (user.estado = body.estado),
      (user.role = body.role);

    user.save((err, userUpdated) => {
      if (err) {
        return res.status(500).json({
          ok: false,
          message:
            "Server didnt understand the URL you gave it, You need will check ID.",
          err,
        });
      }

      if (!userUpdated) {
        res.status(400).json({
          ok: false,
          message:
            "Server didnt understand the URL you gave it, You need will check ID.",
          err,
        });
      }

      return res.status(200).json({
        ok: true,
        message: 'Everything is normal, user updated")',
        user: userUpdated,
      });
    });
  });
}

//=====================================
//        REMOVE USER = DELETE
//=====================================

function deleteUser(req, res) {
  let id = req.params.id;

  User.findByIdAndRemove(id, (err, userDelete) => {
    if (err) {
      return res.status(500).json({
        ok: false,
        message: "Something on the server didnt work right.",
        err,
      });
    }

    if (!userDelete) {
      res.status(400).json({
        ok: false,
        message:
          "Server didnt understand the URL you gave it, You need will check ID.",
      });
    }

    res.status(200).json({
      ok: true,
      message: "Everything is normal, user deleted success",
      user: userDelete,
    });
  });
}
//=====================================
//        POST LOCATION 
//=====================================



//=====================================
//        LOGIN USER = POST
//=====================================

function login(req, res) {
  let body = req.body;

  User.findOne({ email: body.email }, (err, userDB) => {
    if (err) {
      return res.status(500).json({
        ok: false,
        message: "Something on the server didnt work right.",
      });
    }
    if (!userDB) {
      return res.status(400).json({
        ok: true,
        message:
          "Server didnt understand the URL you gave it, You need will check ID, if user exist.", body,
      });
    }
      
    if (!bcrypt.compareSync(body.password, userDB.password)) {
      return res.status(400).json({
        ok: false,
        err: {
          mensaje:
            " Server refuses to give you a file, authentication wont help, your information is not valid",
        },
      });
    }

    
    let token = jwt.sign({
        user: userDB
          
      },process.env.SEED,{ expiresIn: process.env.expiresIn}) // Verify the environment in config

    res.status(200).json({
      ok: true,
      message: "Login was Success",
      userDB,
      token,
      
    });
  });
}




//=====================================
//        TICKET = POST
//=====================================






module.exports = {
  getUsers,
  registerUser,
  deleteUser,
  updateUser,
  login,
};
