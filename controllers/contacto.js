'use strict'

var Contacto = require("../database/models/contacto");
const nodemailer = require("nodemailer");


function postContact(req, res){
    let body = req.body


    let contacto = new Contacto({
        nombre: body.nombre,
        apellido: body.apellido,
        email: body.email,
        telefono: body.telefono,
        mensaje: body.mensaje,
    })

    contacto.save( (err, contactoDB) =>{

        if (err) {
            return res.status(500).json({
              ok: false,
              message: "Something on the server didnt work right.",
              err,
            });
          }
          if (!contactoDB) {
            res.status(404).json({
              ok: false,
              message: "No se publico el contaco",
              contacto: contactoDB,
            });
          }
      

             // async..await is not allowed in global scope, must use a wrapper
    async function main() {
        // Generate test SMTP service account from ethereal.email
        // Only needed if you don't have a real mail account for testing
  
        // create reusable transporter object using the default SMTP transport
        let transporter = nodemailer.createTransport({
          host: "smtpout.secureserver.net",
          port: 465,
          secure: true, // true for 465, false for other ports
          auth: {
            user: process.env.user, // generated ethereal user
            pass: process.env.pass,
            tls: {
              ciphers: 'SSLv3'
              }// generated ethereal password
          },
          
        });
  
        // send mail with defined transport object
        let info = await transporter.sendMail({
          from: body.email, // sender address
          to: "fe9f68e29ec411ea837a2dfad025e955@tickets.tawk.to, geovaneartedesign@gmail.com", // list of receivers
          subject: "Formulario de contacto Super Junker", // Subject line
          html:`
          <h4>Formulario de contacto Super Junker</h4>
          <b>Nombre: </b>  ${body.nombre}<br>
          <b>Apellido: </b> ${body.apellido}<br>
          <b>Emai: </b>  ${body.email}<br>
          <b>Telefono: </b>  ${body.telefono}<br>
          <b>mensaje: </b>  ${body.mensaje}<br>`
  
        });
  
        console.log("Message sent: %s", info.messageId);
        // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
  
        // Preview only available when sending through an Ethereal account
        console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
        // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
      }
  
      main().catch(console.error);



          return res.status(201).json({
            ok: true,
            message: 'Everything is normal ("201 Created")',
            contacto: contactoDB,
          });

    })




    
}





module.exports = {
    postContact
}