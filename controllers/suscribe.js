'use strict'

let Email  = require('../database/models/suscribe')
const nodemailer = require("nodemailer");


function getEmails( req, res){
    let desde = req.query.desde || 0;
    desde = Number(desde);
  
    let limite = req.query.limite || 1000000000000000000;
    limite = Number(limite);
    
  
  
    Email.find({}, 'nombre email')
      .skip(desde)
      .limit(limite)
      .exec((err, emails) => {
        if (err) {
          return res.status(500).json({
            ok: false,
            message:
              "Something on the server didnt work right. Please, You need Verify your information id",
            err,
          });
        }
  
           
           
        
        //Usuario.count({}, (err, counts)=>{ estado para dar os filtros apenas de usuarios ativos
        Email.count({}, (err, counts) => {
        
          return res.status(200).json({
            ok: true,
            emails: emails,
            TotalEmails: counts,
          });
        });
      });

}

function postEmail( req, res){

    let body = req.body

    let email = new Email({

        email: body.email,
        nombre: body.nombre

    });

    
     

    email.save( (err, emailDB)=>{

        if(err){
            res.status(500).json({
                ok: false,
                message:'Error de base de datos',
                err: error
            })
        }
        if(!emailDB){
            res.status(401).json({
                ok: false,
                message:'Error al salvar email',
                err: error
            })
        }


        
        // async..await is not allowed in global scope, must use a wrapper
    async function main() {
        // Generate test SMTP service account from ethereal.email
        // Only needed if you don't have a real mail account for testing
  
        // create reusable transporter object using the default SMTP transport
        let transporter = nodemailer.createTransport({
          host: "smtpout.secureserver.net",
          port: 465,
          secure: true, // true for 465, false for other ports
          auth: {
            user: process.env.user, // generated ethereal user
            pass: process.env.pass,
            tls: {
              ciphers: 'SSLv3'
              }// generated ethereal password
          },
          
        });
  
        let emailcustomer = 'geovaneartedesign@gmail.com'
        // send mail with defined transport object
        let info = await transporter.sendMail({
          from: body.email, // sender address
          to: `${emailcustomer}, ${body.email}`, // list of receivers
          subject: "Inscrito", // Subject line
          html:`
          <h4> Saludos ${body.nombre}, </br>
          Gracias por suscribirte a nuestras ofertas</h4>`,
          inReplyTo:`${body.email}`

  
        });
  
        console.log("Message sent: %s", info.messageId);
        // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
  
        // Preview only available when sending through an Ethereal account
        console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
        // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
      }
  
      main().catch(console.error);



        res.status(200).json({
            ok: true,
            message:'Success registration email',
            suscrito: emailDB
        })



    })


    
    
}


module.exports = {
    postEmail,
    getEmails
}