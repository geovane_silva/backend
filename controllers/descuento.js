'user strict'

let Descuento = require('../database/models/descuento')





//=====================================
//           GET IMAGES
//=====================================


function getImages(req, res) {
    let desde = req.query.desde || 0;
    desde = Number(desde);
  
    let limite = req.query.limite || 5;
    limite = Number(limite);
    
  
    //  Usuario.find({ estado: true },'id name img role email')  nao eliminar usuario, apenas cambiar de estado
  
    Descuento.find({}, 'id  img nombre')
      .skip(desde)
      .limit(limite)
      .exec((err, images) => {
        if (err) {
          return res.status(500).json({
            ok: false,
            message:
              "Something on the server didnt work right. Please, You need Verify your information id",
            err,
          });
        }
  
           
           
        
        //Usuario.count({}, (err, counts)=>{ estado para dar os filtros apenas de usuarios ativos
        Descuento.count({}, (err, counts) => {
          //jqueri para contar usuarios
          return res.status(200).json({
            ok: true,
            images: images,
            TotalImages: counts,
          });
        });
      });
  }








function subirImagen(req, res){
    let body = req.body
    let img = req.files.img
    
    let descuento = new Descuento({

        nombre: body.nombre,
        img: body.img,

    })    

    



    body.img = `uploads/descuentos/${descuento.id}-${img.name}`.replace(/ /g, "")

    descuento.img = body.img

    img.mv(body.img);


    descuento.save( (err, descuentoDB) =>{

        if (err) {
            return res.status(500).json({
              ok: false,
              message: "Something on the server didnt work right.",
              err,
            });
          }
          if (!descuentoDB) {
            res.status(404).json({
              ok: false,
              message: "A file doesnt exist at that address",
              descuentoSAVED: descuentoDB,
            });
          }


         return res.status(201).json({
            ok: true,
            message: 'Everything is normal ("201 Created")',
            descuento: descuentoDB,
          });
    })

    
    


   // foto_frontal.mv(body.foto_frontal);

}




module.exports = {
    subirImagen,
    getImages
}