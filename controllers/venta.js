'use strict'

var Venta = require("../database/models/venta");
const nodemailer = require("nodemailer");

const fs = require('fs');  // metodo para borrar imagen y no dejar duplicar (hay que tener cuidado y hacer pruebas porque no es compatible en todas las verciones).





function getVentas( req, res){

    let desde = req.query.desde || 0;
  desde = Number(desde);

  let limite = req.query.limite || 5;
  limite = Number(limite);


  //  Usuario.find({ estado: true },'id name img role email')  nao eliminar usuario, apenas cambiar de estado

  Venta.find({}, 'nombre apellido  email telefono marca modelo ano description_transmision precio_venta foto_frontal foto_trasera foto_izquierda foto_derecha foto_motor foto_interior fotos role')
    .skip(desde)
    .limit(limite)
    .exec((err, ventas) => {
      if (err) {
        return res.status(500).json({
          ok: false,
          message:
            "Something on the server didnt work right. Please, You need Verify your information id",
          err,
        });
      }




      //Usuario.count({}, (err, counts)=>{ estado para dar os filtros apenas de usuarios ativos
      Venta.count({}, (err, counts) => {
        //jqueri para contar usuarios
        return res.status(200).json({
          ok: true,
          ventas: ventas,
          Totalventas: counts,
        });
      });
    });

}

  function postVenta(req, res){

    //Use the name of the input field (i.e. "avatar") to retrieve the uploaded file
    let body = req.body

  
let all_fotos = [];
all_fotos.push(req.files);
console.log(all_fotos)

let foto_frontal = all_fotos[0].foto_frontal;
let foto_trasera = all_fotos[0].foto_trasera;
let foto_izquierda = all_fotos[0].foto_izquierda;
let foto_derecha = all_fotos[0].foto_derecha;
let foto_motor = all_fotos[0].foto_motor;
let foto_interior = all_fotos[0].foto_interior;


 //Use the mv() method to place the file in upload directory (i.e. "uploads")

 

    
//    console.log('aqui estan todas', fotos); // organiza esse teu codigo heimman, tá feio


   
    var venta = new Venta({
      nombre: body.nombre,
      apellido: body.apellido,
      email: body.email,
      telefono: body.telefono,
      marca: body.marca,
      modelo: body.modelo,
      ano: body.ano,
      description_transmision: body.description_transmision,
      precio_venta: body.precio_venta,
      foto_frontal: body.foto_frontal,
      foto_trasera: body.foto_trasera,
      foto_izquierda: body.foto_izquierda,
      foto_derecha: body.foto_derecha,
      foto_motor: body.foto_motor,
      foto_interior: body.foto_interior,
      role: body.role,
    });
    
      body.foto_frontal = `uploads/ventas/${venta.id}-${foto_frontal.name}`

      body.foto_frontal = `uploads/ventas/${venta.id}-${foto_frontal.name}`
      body.foto_trasera = `uploads/ventas/${venta.id}-${foto_trasera.name}`
      body.foto_izquierda = `uploads/ventas/${venta.id}-${foto_izquierda.name}`
      body.foto_derecha = `uploads/ventas/${venta.id}-${foto_derecha.name}`
      body.foto_motor = `uploads/ventas/${venta.id}-${foto_motor.name}`
      body.foto_interior = `uploads/ventas/${venta.id}-${foto_interior.name}`
     
  


        
      venta.foto_frontal = body.foto_frontal
      venta.foto_trasera = body.foto_trasera
      venta.foto_izquierda = body.foto_izquierda
      venta.foto_derecha = body.foto_derecha
      venta.foto_motor = body.foto_motor
      venta.foto_interior = body.foto_interior


      foto_frontal.mv(body.foto_frontal);
      foto_trasera.mv(body.foto_trasera);
      foto_izquierda.mv(body.foto_izquierda);
      foto_derecha.mv(body.foto_derecha);
      foto_motor.mv(body.foto_motor);
      foto_interior.mv(body.foto_interior);


    venta.save((err, ventaDB)=>{
      if (err) {
        return res.status(500).json({
          ok: false,
          message: "Something on the server didnt work right.",
          err,
        });
      }
      if (!ventaDB) {
        res.status(404).json({
          ok: false,
          message: "A file doesnt exist at that address",
          venta: ventaDB,
        });
      }


        // async..await is not allowed in global scope, must use a wrapper
    async function main() {
      // Generate test SMTP service account from ethereal.email
      // Only needed if you don't have a real mail account for testing

      // create reusable transporter object using the default SMTP transport
      let transporter = nodemailer.createTransport({
        host: "smtpout.secureserver.net",
        port: 465,
        secure: true, // true for 465, false for other ports
        auth: {
          user: process.env.user, // generated ethereal user
          pass: process.env.pass,
          tls: {
            ciphers: 'SSLv3'
            }// generated ethereal password
        },
        
      });

      // send mail with defined transport object
      let info = await transporter.sendMail({
        from: body.email, // sender address
        to: "process.environments@gmail.com, manuelemilio@live.com, sales@junkerbernird.com", // list of receivers
        subject: "FORMULARIO DE VENDENOS TU AUTO", // Subject line
        html:`
        <b>Nombre: </b>  ${body.nombre}<br>
        <b>Spellido: </b> ${body.apellido}<br>
        <b>Emai: </b>  ${body.email}<br>
        <b>Telefono: </b>  ${body.telefono}<br>
        <b>Marca: </b> ${body.marca}<br>
        <b>Modelo: </b> ${body.modelo}<br>
        <b>Ano: </b> ${body.ano}<br>
        <b>Precio de venta: </b> ${body.precio_venta}<br>`, // plain text body
               attachments: [
                {   // utf-8 string as an attachment
                  foto_frontal: 'foto_frontal',
                  path: `${process.env.URL_SITE}/${venta.foto_frontal}`,


                }, {   // utf-8 string as an attachment
                  foto_trasera: 'foto_trasera',
                  path: `${process.env.URL_SITE}/${venta.foto_trasera}`,


                },
                {   // utf-8 string as an attachment
                  foto_izquierda: 'foto_izquierda',
                  path: `${process.env.URL_SITE}/${venta.foto_izquierda}`,


                },
                {   // utf-8 string as an attachment
                  foto_derecha: 'foto_derecha',
                  path: `${process.env.URL_SITE}/${venta.foto_derecha}`,


                },
                {   // utf-8 string as an attachment
                  foto_motor: 'foto_motor',
                  path: `${process.env.URL_SITE}/${venta.foto_motor}`,


                },
                {   // utf-8 string as an attachment
                  foto_interior: 'foto_interior',
                  path: `${process.env.URL_SITE}/${venta.foto_interior}`,


                }],

      });

      console.log("Message sent: %s", info.messageId);
      // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

      // Preview only available when sending through an Ethereal account
      console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
      // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
    }

    main().catch(console.error);


               res.status(201).json({
                ok: true,
                message: 'Everything is normal ("201 Created")',
                venta: ventaDB,
              });
        });


      }




module.exports = {
    getVentas,
    postVenta,
}
